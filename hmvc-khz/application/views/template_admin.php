 <!DOCTYPE html>
 <html lang="en">
     <head>
         <title></title>
         <meta charset="UTF-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
         <link href="css/style.css" rel="stylesheet">
     </head>
     <body>
    <nav class="navbar navbar-default navbar-fixed-top">
    <?php get_header(); ?>
    </nav> 
        <?php echo $contents; ?>
        <div id="footer">
<?php get_footer(); ?>
            
        </div>
     </body>
 </html>
 <!-- Main content -->

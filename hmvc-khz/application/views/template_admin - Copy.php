<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Starter</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/AdminLTE/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/selectize.default.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/AdminLTE/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/AdminLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/AdminLTE/dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Jquery -->
  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url(); ?>assets/Jquery/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url();?>assets/Jquery/currencyFormatter.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/cleave.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/numeral.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/selectize.js" type="text/javascript"></script>
  
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?php echo base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><i class="fa fa-home"></i></b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Home</b></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="<?php $gambar = !empty($data_admin->image)?base_url().'uploads/'.$data_admin->image:base_url().'assets/AdminLTE/dist/img/avatar.png'; echo $gambar; ?>" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"><?php echo $username = !empty($data_admin->username)?$data_admin->username:'Anonymous'; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="<?php $gambar = !empty($data_admin->image)?base_url().'uploads/'.$data_admin->image:base_url().'assets/AdminLTE/dist/img/avatar.png'; echo $gambar; ?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $username = !empty($data_admin->username)?$data_admin->username:'Anonymous'; ?>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                 </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $gambar = !empty($data_admin->image)?base_url().'uploads/'.$data_admin->image:base_url().'assets/AdminLTE/dist/img/avatar.png'; echo $gambar; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $username = !empty($data_admin->username)?$data_admin->username:'Anonymous'; ?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <!-- Optionally, you can add icons to the links -->
        <?php
        if (isset($this->ion_auth->get_users_groups()->row()->id)) {
          if ($this->ion_auth->get_users_groups()->row()->id == 1) {
        ?>
          <!-- MENAMPILKAN MENU ADMINISTRATOR -->
          <li class="active"><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
          <li class="treeview">
            <a href="#"><i class="fa fa-gear"></i> <span>Setting</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>admin/konfigurasi"><i class="fa fa-config"></i> Konfigurasi</a></li>
              <li><a href="<?php echo base_url(); ?>m_meja"><i class="fa fa-table"></i> Setting Meja</a></li>
              <li><a href="<?php echo base_url(); ?>m_metode_pembayaran"><i class="fa fa-bill"></i> Metode Pembayaran</a></li>
              <li><a href="<?php echo base_url(); ?>admin/change_password"><i class="fa fa-lock"></i> Change Password</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#"><i class="fa fa-users"></i> <span>Users</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>admin/excel_user"><i class="fa fa-user"></i> Data User</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="<?php echo base_url('m_product'); ?>"><i class="fa fa-cubes"></i> <span>Produk</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
          </li>
          <li class="treeview">
            <a href="#"><i class="fa fa-list"></i> <span>Kategori dan Jenis</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url('m_kategori');?>">Kategori</a></li>
              <li><a href="<?php echo base_url('m_jenis'); ?>">Jenis</a></li>
            </ul>
          </li>
          
          <li class="treeview">
            <a href="#"><i class="fa fa-cash"></i> <span>Kasir</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>kasir"> Menu Kasir Utama</a></li>
              <li><a href="<?php echo base_url(); ?>kasir/status_kasir"> Status Kasir</a></li>
             
            </ul>
          </li>
          
          <li class="treeview">
            <a href="#"><i class="fa fa-book"></i> <span>Laporan Umum</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>daftar_struk"> Daftar Struk</a></li>
              <li><a href="<?php echo base_url(); ?>laporan"> Laporan Penjualan</a></li>
              <li><a href="#"> Laporan Sift</a></li>
              <!-- <li><a href="<?php echo base_url(); ?>admin/daftar_customer"> Daftar Customer</a></li> -->
            </ul>
          </li>
          
          <li class="treeview">
            <a href="#"><i class="fa fa-book"></i> <span>Akuntansi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>tutup_buku"> Tutup Buku</a></li>
              <li><a href="<?php echo base_url(); ?>m_coa"> COA</a></li>
              <li><a href="<?php echo base_url(); ?>m_kelompok_coa">Kelompok COA</a></li>
              <li><a href="<?php echo base_url(); ?>m_akuntansi_kategori">Kategori COA</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi">Menu Utama Akuntansi</a></li>
              <!-- <li><a href="<?php echo base_url(); ?>akuntansi/kasdanbank"> Kas Dan Bank</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/jurnalumum"> Jurnal Umum</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/neraca"> Neraca</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/labarugi"> Laba Rugi</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/labaditahan"> Laba ditahan</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/aruskas"> Aruskas</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/buku_besar"> Buku Besar</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/bukubesarpembantu"> Buku Besar Pembantu</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/laporanjurnal"> Laporan Jurnal</a></li> -->
            </ul>
          </li>
          
          
          
          <!-- <li><a href="<?php echo base_Url('admin/pemesanan'); ?>"><i class="fa fa-cart-arrow-down"></i> Pemesanan</a></li> -->
          <!-- <li class="treeview">
            <a href="#"><i class="fa fa-user"></i> <span>Akuntansi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>akuntansi"><i class="fa fa-user"></i> Home Akuntansi</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/akuntansi_akun"><i class="fa fa-user"></i> Data Akun</a></li>
              <li><a href="<?php echo base_url(); ?>akuntansi/akuntansi_kategori_akun"><i class="fa fa-list"></i> Data Kategori</a></li>
            </ul>
          </li> -->
          <!-- <li class="treeview">
            <a href="#"><i class="fa fa-tags"></i> <span>Kategori</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="<?php echo base_url(); ?>admin/insert_kategori"><i class="fa fa-plus"></i> Tambah Kategori</a></li>
              <li><a href="<?php echo base_url(); ?>admin/kategori/"><i class="fa fa-tag"></i> Data Kategori</a></li>
            </ul>
          </li>
 -->
        <?php
          } elseif ($this->ion_auth->get_users_groups()->row()->id == 2) {
        ?>
            <li class="active"><a href="<?php echo base_url(); ?>admin/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <?php
          } elseif ($this->ion_auth->get_users_groups()->row()->id == 3) {
        ?>
            <li class="active"><a href="<?php echo base_url(); ?>waiters/pemesanan"><i class="fa fa-shopping-cart"></i> Pemesanan</a></li>
        <?php
          }
        }
        ?>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">

      <!-- Your Page Content Here -->
      <div class="row">
        <?php echo $contents; ?>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>
</div>
<?php

// Untuk mendapatkan Top Rank Product
if (!empty($topRankProduct)) {
  $tampung = array();
  $tampung_product = array();
  foreach ($topRankProduct as $key => $resultTop) {
      $tampung[] = $resultTop->top_rank;
      $tampung_product[] = $resultTop->nama_product;
  }
}
$val_tampung = !empty($tampung)?implode(',',$tampung):null;
?>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";
</script>


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/AdminLTE/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- CUSTOM JS -->
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<!-- SPARKLINE -->
<script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/AdminLTE/dist/js/app.min.js"></script>
<script type="text/javascript">

</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
</body>
</html>

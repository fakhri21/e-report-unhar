<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>
        <?php echo $title; ?>
        </title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/bootstrap/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/alert/css/alertify.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/bootstrap/js/html5shiv.min.js"></script>
        <script src="<?php echo base_url();?>assets/bootstrap/js/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url();?>assets/Jquery/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <!-- Navigasi -->
        <div id="navigasi">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url();?>home"><img src="<?php echo base_url(); ?>assets/bootstrap/img/logo.jpg"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            
                            <!--Untuk Task bar -->
                            <li>
                                <?php if ($this->ion_auth->logged_in()) {
                                $userid=$this->ion_auth->user()->row()->user_id;
                                $usergroup=$this->ion_auth->get_users_groups()->row()->name;
                                echo "<a href='".base_url()."".$usergroup."'><i class='fa fa-user-o'></i> ".$this->ion_auth->user()->row()->username."</a>";
                                }
                                else {
                                echo "<li><a href='user'><i class='fa fa-user-o'></i> Login</a></li>";
                                echo "<li><a href='register'><i class='fa fa-user-o'></i> Register</a></li>";
                                }
                                ?>
                                
                            </li>
                            <li>
                                <?php if ($this->ion_auth->logged_in()){
                                
                                echo "<a href='".base_url()."".$usergroup."/logout'><i class='fa fa-sign-out'></i> Logout</a>";
                                } ?>
                            </li>
                            <li><a href="<?php echo base_url();?>cart"><i class="fa fa-shopping-cart"></i> Keranjang</a></li>
                            <?php if ($this->ion_auth->logged_in() ) { 
                            if ($this->ion_auth->get_users_groups()->row()->id==3) {
                            ?>                          
                            <li><a href="<?php echo base_url();?>meja"><i class="fa fa-th-large"></i> Meja</a></li>
                            <?php }} ?>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <?php
                ?>
                <!-- /.container-fluid -->
            </nav>
        </div>
        <!-- #navigasi -->
        <!-- Content here -->
        <?php echo $contents; ?>
        <!-- /content -->
        <!-- the footer -->
		
        <div id="footer">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-6">
                            <h4>About us</h4>
                            <p>Coffee Blues Indonesia adalah gerai kopi yang menyajikan minuman kopi secara manual brew &amp; non kopi serta makanan dengan cita rasa tinggi. Kami menyediakan roast bean arabica gayo takengon kualitas terbaik dengan varian arabica specialty, peaberry ( kopi jantan), wine, luwak, natural proses, full washed, green coffee dan robusta. Kehangatan, keakraban dan kekeluargaan adalah kami. Temukan petualangan hidup anda bersama Coffee Blues Indonesia.
                            </p>
                        </div>
                        <div class="col-md-4 col-sm-6 text-right ">
                            
                            <h4>Our Social</h4>
                            
                            <a href="https://web.facebook.com/CoffeeBluesGOR/" class="btn btn-primary btn-xs"><i class="fa fa-facebook-square"></i> Facebook</a>
                            <a href="https://twitter.com/" class="btn btn-info btn-xs"><i id="social-tw" class="fa fa-twitter-square"></i> Twitter</a>
                            <a href="mailto:coffeebluesindonesia@gmail.com" class="btn btn-success btn-xs"><i class="fa fa-envelope-square"></i> Email Us</a><br>
                            <a href="https://www.instagram.com/coffeebluesindonesiagayo/" class="btn btn-danger btn-xs" style="margin-top: 6px;"><i class="fa fa-instagram"></i> Instagram</a>
                            <a href="http://coffeebluesgayo.blogspot.co.id/" class="btn btn-warning btn-xs" style="margin-top: 6px;"><i class="fa fa-rss"></i> Blogger</a>
                            <a href="https://www.google.co.id/maps/place/Coffee+BLUES,+Medan:+Gayo+Original+Roasted/@3.537075,98.6892283,17z/data=!4m12!1m6!3m5!1s0x3031306f9e6ab9ed:0xbc2b7c469c7e0a89!2sCoffee+BLUES,+Medan:+Gayo+Original+Roasted!8m2!3d3.537075!4d98.691417!3m4!1s0x3031306f9e6ab9ed:0xbc2b7c469c7e0a89!8m2!3d3.537075!4d98.691417?hl=en" class="btn btn-primary btn-xs" style="margin-top: 6px;" target="blank"><i class="fa fa-map"></i> Maps</a>
                            
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="text-justify" style="border-top: 1px solid #ccc; margin-top: 10px; padding-top: 5px;">
                                <small><strong>Copyright &copy; 2017. <a href="http://coffeebluesindonesia.com" style="color: #efe481;">CoffeeBlues</a> All right Reserved,</strong></small>
                                <small><strong></small>
								<br>
                                <small><strong>Proudly and Designed By <a href="https://www.facebook.com/groups/wearekhz"><span></span>KHz Technology</a></strong></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of footer -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>assets/alert/js/alertify.js"></script>
    </body>
</html>
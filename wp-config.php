<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ereport');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'SvZvx9iVug/9/RqF(vKoKh5C8~:g/_VmQ1-&7=gM)+]Mq_X[7+=Ci+xXV:i94ZCT');
define('SECURE_AUTH_KEY',  '^uHz{Y4V1]FuJ>~F8-aq(,{_K(XLB&L1B1+zwv%h#BgvZoA+giHYq/+B`G3a__0B');
define('LOGGED_IN_KEY',    'W!JQ`;,k]+~j>MJ*U]:3O72C0W))*XMjr:#/YTGD>#HY7wEFffdy&Qo!4I6{%_}f');
define('NONCE_KEY',        '>0u18=Yd$__I8WqN=[4YU*D`cBQ;LwXdr|!v+b%,86U!;M.&Y}Vot&E^hSpH|@i2');
define('AUTH_SALT',        'i7n4Jpv{fn$Y(4-L:/ZAIA1&0*(wr.Wfe$] T-q7^14PK>-rmsf@4;#J$L:%AcI:');
define('SECURE_AUTH_SALT', 'KQOn+JVF=$4Zr&)&0kb$;o[^8]fVl-P{|kLpdJNzyUXm2@S|W/ |eUN,-Z!*}XNi');
define('LOGGED_IN_SALT',   '@.c!F,]3^@P:k$zMS%^/194EX+`~;sNc_| kcByXSv`xh-p?$pzNkI$fC1ngY (a');
define('NONCE_SALT',       'M.L!9tzeC^vANd$mGhIj[Z$?A3ZY$Z48euqC[+!paJh?hy?8$ww;p-O!|HzPj8@k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_tes2';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
